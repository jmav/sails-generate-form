<div ng-form="<%= model %>Form" layout="row" layout-sm="column" layout-wrap>
  <% for(var attribute in modelAttributes) { var options = modelAttributes[attribute]; %>
    <% if(options.label) {%>
      <% if(options.type === 'string' && !options.enum) {%>
        <md-input-container flex="50" flex-sm="100">
            <label><%=options.label%></label>
            <input <%=options.size ? 'md-maxlength='+options.size : '' %> <%=options.required ? 'required' : ''%> name="<%= attribute %>" ng-model="item.<%= attribute %>">
            <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </md-input-container>
      <% } %>
      <% if(options.type === 'string' && options.enum) {%>
        <md-select-container flex="50" flex-sm="100">
          <label><%=options.label%></label>
          <md-select <%=options.required ? 'required' : ''%> name="<%= attribute %>" ng-model="item.<%= attribute %>">
            // <md-option ng-value="opt.value" ng-repeat='opt in [{value: "m", title: "Male"},{value: "f", title: "Female"}]'>{{ opt.title }}</md-option>
            <md-option ng-value="opt" ng-repeat='opt in <%= JSON.stringify(options.enum) %>'>{{ opt }}</md-option>
          </md-select>
          <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </md-select-container>
      <% } %>
      <% if(options.type === 'integer') {%>
        <md-input-container flex="50" flex-sm="100">
            <label><%=options.label%></label>
            <input type="number" <%=options.size ? 'maxlength='+options.size : '' %> <%=options.required ? 'required' : ''%> name="<%= attribute %>" ng-model="item.<%= attribute %>">
            <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </md-input-container>
      <% } %>
      <% if(options.type === 'datetime') {%>
        <md-input-container flex="50" flex-sm="100">
            <label><%=options.label%></label>
            <input type="text" <%=options.required ? 'required' : ''%>
             ng-model="item.<%= attribute %>"
             name="<%= attribute %>"
             pick-a-date="date"
             placeholder="Select Date"
             pick-a-date-options="{ format: 'yyyy-mm-dd' }" />
            <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </md-input-container>
      <% } %>
      <% if(options.type === 'text') {%>
        <!-- http://ngmodules.org/modules/textAngular -->
        <div flex="50" flex-sm="100">
            <label><%=options.label%></label>
            <div text-angular <%=options.required ? 'required' : ''%> name="<%= attribute %>" ng-model="item.<%= attribute %>"></div>
            <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </div>
      <% } %>
      <% if(options.type === 'boolean') {%>
        <md-input-container flex="50" flex-sm="100">
            <md-switch ng-model="item.<%= attribute %>" aria-label="<%=options.label%>">
              <%=options.label%>
            </md-switch>
            <div ng-messages="<%= model %>Form.<%= attribute %>.$error" ng-messages-include="/app/common/templates/error-messages.html"></div>
        </md-input-container>
   <% }}} %>
  <div flex="100">
    <md-button ng-click="save()" ng-disabled="<%= model %>Form.$invalid" class="md-raised md-primary">Save</md-button>
  </div>
</div>
