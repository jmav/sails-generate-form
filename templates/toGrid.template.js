<h1><%= model %> grid</h1>

<table tr-ng-grid="" items="items" page-items="20" >

    <thead>
        <tr>
            <% for(var attribute in modelAttributes) { var options = modelAttributes[attribute]; if(options.label) { %>
            <th field-name="<%= attribute %>" display-name="<%=options.label%>" enable-sorting="false" enable-filtering="false"></th>
            <% }} %>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td field-name="id">
              <md-button ng-click="selectRoute('codelists.subcategories.detail', {codeList: codeListId, id: gridDisplayItem.id})" >Edit</md-button>
            </td>
        </tr>
    </tbody>

</table>
